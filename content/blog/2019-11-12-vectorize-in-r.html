---
title: Vectorize in R
author: Jason Mercer
date: '2019-11-14'
slug: vectorize-in-r
featured: veccomp-1.png
featuredpath: ../blog/2019-11-12-vectorize-in-r_files/figure-html
categories:
  - R
tags:
  - tutorial
  - optimization
description: 'Letting the Vectorize function do the heavy-lifting when it comes to applying complicated control flows.'
linktitle: ''
type: post
output:
  blogdown::html_page:
    toc: true
---


<div id="TOC">
<ul>
<li><a href="#vectorization-in-r">Vectorization in R</a></li>
<li><a href="#the-vectorize-function">The Vectorize function</a></li>
<li><a href="#vectorize-in-action">Vectorize in action</a><ul>
<li><a href="#for-loop-solution">For loop solution</a></li>
<li><a href="#vectorized-solution">Vectorized solution</a></li>
</ul></li>
<li><a href="#speed">Speed</a><ul>
<li><a href="#benchmarking">Benchmarking</a></li>
</ul></li>
<li><a href="#bonus">Bonus</a><ul>
<li><a href="#vectorize-as-a-function-factory">Vectorize as a function factory</a></li>
<li><a href="#other-ways-of-applying-conditions-in-r">Other ways of applying conditions in R</a></li>
</ul></li>
</ul>
</div>

<div id="vectorization-in-r" class="section level2">
<h2>Vectorization in R</h2>
<p>One of the cool things about R is that it is a vectorized language. This means that you can apply a vector (i.e., a series of values) to functions like <code>sqrt</code> and <code>log</code> without the need to write a for loop. While I was naive to how useful this was when learning R, in retrospect, I now very much appreciate this behavior.</p>
<p>Not every function has an obvious path towards vectorization, however. One such problem path occurs when control flow is involved. That is, when functions like <code>if</code> and <code>else</code> are used to evaluate different condtions. These functions can only handle one value at a time. For example, if you wanted to know if one value equaled another, you can check this condition for one condition (note: this is an illustrative example; there are better ways of checking this condition):</p>
<pre class="r"><code>if(1 == 0) {
  &quot;Equal&quot;
} else {
  &quot;Not equal&quot;
}</code></pre>
<pre><code>## [1] &quot;Not equal&quot;</code></pre>
<p>However, the moment you apply this logic to a vector with length greater than 1, you no longer get the expected or desired result:</p>
<pre class="r"><code>if(c(1, 0) == 0) {
  &quot;Equal&quot;
} else {
  &quot;Not equal&quot;
}</code></pre>
<pre><code>## Warning in if (c(1, 0) == 0) {: the condition has length &gt; 1 and only the first
## element will be used</code></pre>
<pre><code>## [1] &quot;Not equal&quot;</code></pre>
<p>One way of getting around the problem of more than one value in if-else statements is to use their vectorized equivalent, namely <code>ifelse</code> from base R or <code>if_else</code> from dplyr. However, while these functions are great for relatively simple conditions, they can get cumbersome to read and write when conditions are nested. They also can’t handle else-if conditions. For example, I find the following difficult to read and simulaneously limited in scope (but maybe that’s just me):</p>
<pre class="r"><code>ifelse(
  test = `condition 1`,
  yes = X,
  no = ifelse(
    test = `condition 2`,
    yes = Y,
    no = ifelse(
      ...
      )
    )
  )
)</code></pre>
<p>So, wouldn’t it be nice if we could somehow combine more complicated control flows with vectorization?</p>
<p>Enter: <code>Vectorize</code>.</p>
</div>
<div id="the-vectorize-function" class="section level2">
<h2>The Vectorize function</h2>
<p>What <code>Vectorize</code> does is it takes a function and generates a vectorized equivalent to that function, allowing us to apply a much more complicated control logic to a vector without having to do the heavy lifting of having a write a for loop, use the <code>*apply</code> family of functions, or the <code>map</code> functions in purrr. This behavior makes it much easier to apply such functions to functions like <code>mutate</code> in dplyr.</p>
<p>As an aside, <code>Vectorize</code> also represents another neat aspect of R: functional programming. That is, <code>Vectorize</code> takes a function and uses functions to return another function. To me, these “function factories” are kind of mind blowing. For more on function factories, check out <a href="https://adv-r.hadley.nz/function-factories.html">Hadley Wickham’s chapter</a> on the topic.</p>
<p>But let’s check out an example of how we could use <code>Vectorize</code> on some problem.</p>
</div>
<div id="vectorize-in-action" class="section level2">
<h2>Vectorize in action</h2>
<p>As an example, I’m going focus on a common application in isotope hydrology. The theoretical details of this problem are beyond the scope of this post, and I’ve simplified the issue quite a bit, but suffice it to say that the problem involves inputing a temperature, which then returns a value near 1. However, the result is conditional on a number of factors, such as the isotope involved, phase (e.g, liquid, gas), etc. In the example below I’m using a simplified form of this problem to reduce complexity.</p>
<p>First, I need to define two functions for when temperatures are warm or cold:</p>
<pre class="r"><code>warm &lt;- function(TK) { #TK: Temperature in Kelvin
  a &lt;- 24.844
  b &lt;- -76.248
  c &lt;- 52.612
  first &lt;- a * ((1e+06) / ((TK^2)))
  second &lt;- b * (1e+03)/(TK)
  exp((first + second + c) / 1000)
}

cold &lt;- function(TK) {
  a &lt;- 24.844
  b &lt;- -76.248
  c &lt;- 71.912
  first &lt;- a * ((1e+06) / ((TK^2)))
  second &lt;- b * (1e+03)/(TK)
  exp((first + second + c) / 1000)
}</code></pre>
<p>Now to define our function containing the control logic.</p>
<div id="for-loop-solution" class="section level3">
<h3>For loop solution</h3>
<p>When I first wrote up this problem I didn’t know about the <code>Vectorize</code> function so I wrote it as a for loop. I think some are more comfortable with that kind of solution, so I’m going to present it for comparison purposes:</p>
<pre class="r"><code>equil_frac_loop &lt;- function(Ta, verbose = FALSE){
  frac_factor &lt;- numeric()
  for(i in 1:length(Ta)) {
    if(Ta[i] &gt; 100 &amp; verbose) {
      warning(paste(&#39;Temperature values are outside the bounds of the method -&#39;,
        &#39;may generate anamolous data.&#39;))
    }
    temp_K &lt;- Ta[i] + 273.15 #Convert C to K
    if(Ta[i] &gt;= 0) {
      frac_factor[i] &lt;-  warm(temp_K)
    } else if(Ta[i] &lt; 0) {
      frac_factor[i] &lt;-  cold(temp_K)
    } else {
      stop(&quot;Something&#39;s wrong with your temperature values.&quot;)
    }
  }
  frac_factor
}</code></pre>
</div>
<div id="vectorized-solution" class="section level3">
<h3>Vectorized solution</h3>
<p>To generate the vectorized solution, we first write the unvectorized “skeleton” to our problem:</p>
<pre class="r"><code>equil_frac_unvec &lt;- function(Ta, verbose = FALSE){
  if(Ta &gt; 100 &amp; verbose) {
    warning(paste(&#39;Temperature values are outside the bounds of the method -&#39;,
      &#39;may generate anamolous data.&#39;))
  }
  temp_K &lt;- Ta + 273.15 #Convert C to K
  if(Ta &gt;= 0) {
    return(warm(temp_K))
  } else if(Ta &lt; 0) {
    return(cold(temp_K))
  } else {
    stop(&quot;Something&#39;s wrong with your temperature values.&quot;)
  }
}</code></pre>
<p>Now, here is where the magic happens. To vectorize the above function, all we need to do is wrap it with <code>Vectorize</code> and save it as a new object.</p>
<pre class="r"><code>equil_frac_vec &lt;- Vectorize(equil_frac_unvec)</code></pre>
<p>And that’s it! Our unvectorized function is now vectorized. But do the for loop and vectorized forms generate the same solution? Let’s check the outputs with the <code>all.equal</code> function, which for a numeric vector, will check that each element in a “target” and “current” vector are the same.</p>
<pre class="r"><code>#First let&#39;s generate some &quot;temperatures&quot;.
temperatures &lt;- runif(n = 20, min = -30, max = 30)
#Apply the two solutions.
vec_out &lt;- equil_frac_vec(Ta = temperatures)
loop_out &lt;- equil_frac_loop(Ta = temperatures)
#Test if they are equivalent; this will return TRUE if all values are the same.
all.equal(loop_out, vec_out)</code></pre>
<pre><code>## [1] TRUE</code></pre>
<p>Well, look at that! <code>all.equal</code> returned <code>TRUE</code>, meaning both methods produced the same results.</p>
</div>
</div>
<div id="speed" class="section level2">
<h2>Speed</h2>
<p>Now that we have some different forms of the same control logic, the question becomes: Which is faster? Optimizing such a simple function probably won’t matter much, but in other cases it could be important.</p>
<p>For benchmarking I’m using the <code>bench</code> package, which can be used to generate some really nice insights into function performance.</p>
<p>For the sake of comparison, I’m also going to include a solution that uses the unvectorized form of the function, wrapped with <code>base::sapply</code> and <code>purrr::map_dbl</code>, which should generate the same results as the other two functions. But first let’s load some libraries that will be useful in the comparison.</p>
<pre class="r"><code>library(bench)</code></pre>
<pre><code>## Warning: package &#39;bench&#39; was built under R version 3.6.2</code></pre>
<pre class="r"><code>library(tidyverse)</code></pre>
<pre><code>## Warning: package &#39;tidyverse&#39; was built under R version 3.6.2</code></pre>
<pre><code>## Warning: package &#39;tidyr&#39; was built under R version 3.6.2</code></pre>
<pre><code>## Warning: package &#39;dplyr&#39; was built under R version 3.6.2</code></pre>
<div id="benchmarking" class="section level3">
<h3>Benchmarking</h3>
<p>This first bit of code just tells <code>bench</code> which functions to run and how many times.</p>
<pre class="r"><code>vector_compare &lt;- bench::mark(
  `Apply family` = sapply(temperatures, equil_frac_unvec),
  `Purrr family` = map_dbl(temperatures, equil_frac_unvec),
  `Vectorize` = equil_frac_vec(temperatures),
  `For loop` = equil_frac_loop(temperatures),
  iterations = 10000
)</code></pre>
<p>Eazy peazy! So, what do the results look like (after removing some things that we don’t care as much about for the purposes of this post)?</p>
<pre class="r"><code>vector_compare %&gt;%
  select(expression, min, median, `itr/sec`, mem_alloc, total_time) %&gt;%
  arrange(median) %&gt;%
  mutate(expression = names(expression))</code></pre>
<pre><code>## # A tibble: 4 x 5
##   expression        min   median `itr/sec` mem_alloc
##   &lt;chr&gt;        &lt;bch:tm&gt; &lt;bch:tm&gt;     &lt;dbl&gt; &lt;bch:byt&gt;
## 1 For loop       27.6us   32.1us    28704.      792B
## 2 Apply family   47.4us   53.3us    17074.      720B
## 3 Purrr family   50.9us   57.3us    16686.    4.23KB
## 4 Vectorize      71.3us   79.6us    11917.      720B</code></pre>
<p>Fascinating! It looks like the for loop function was fastest, while the vectorize function was slowest. Relatively, the for loop function was ~ 2 times faster than the vectorize function, on average (median).</p>
<p>But how do the results look? The image below provides some indication. It looks like the distributions of the processing times are pretty scattered (hence the need to log the scale), but the medians appear fairly representative. This, then, suggests that it is really the for loop function we’d want to use, if performance became a big issue. Or, given <a href="../../blog/benchmarking-r-and-rcpp/">my exploration of Rcpp</a>, we might want to consider writing the function in C++. But we’d need to run the code a lot to justify the work required for these alternative strategies.</p>
<pre class="r"><code>vector_compare %&gt;%
  unnest(cols = c(time, gc)) %&gt;%
  select(expression, time, median) %&gt;%
  arrange(median) %&gt;%
  mutate(expression = fct_inorder(expression)) %&gt;%
  mutate(time = as.numeric(time)) %&gt;%
  ggplot(aes(expression, time, fill = expression)) +
  geom_violin(show.legend = FALSE) +
  labs(x = NULL,
    y = &quot;Log time (s)&quot;) +
  scale_y_log10() +
  theme_bw() +
  scale_fill_brewer(palette = &quot;Set1&quot;, direction = -1)</code></pre>
<p><img src="/blog/2019-11-12-vectorize-in-r_files/figure-html/veccomp-1.svg" width="672" /></p>
<p>For more on <code>Vectorize</code>, check out <a href="https://deanattali.com/blog/mutate-non-vectorized/">this post</a> by Dean Attali.</p>
</div>
</div>
<div id="bonus" class="section level2">
<h2>Bonus</h2>
<div id="vectorize-as-a-function-factory" class="section level3">
<h3>Vectorize as a function factory</h3>
<p>I mentioned earlier that <code>Vectorize</code> is taking advantage of R’s functional programming infrastructure. But how is it doing that? Well, that’s a bit beyond the scope of this post, but I do think it interesting to see what the internals of our new vectorized function look like:</p>
<pre class="r"><code>equil_frac_vec</code></pre>
<pre><code>## function (Ta, verbose = FALSE) 
## {
##     args &lt;- lapply(as.list(match.call())[-1L], eval, parent.frame())
##     names &lt;- if (is.null(names(args))) 
##         character(length(args))
##     else names(args)
##     dovec &lt;- names %in% vectorize.args
##     do.call(&quot;mapply&quot;, c(FUN = FUN, args[dovec], MoreArgs = list(args[!dovec]), 
##         SIMPLIFY = SIMPLIFY, USE.NAMES = USE.NAMES))
## }
## &lt;environment: 0x0000000015b5d288&gt;</code></pre>
<p>That looks nothing like my original code! So something really fascinating is going on to turn the unvectorized function into that crazy set of lines above. But I’ll leave the exploration of that process for another post.</p>
</div>
<div id="other-ways-of-applying-conditions-in-r" class="section level3">
<h3>Other ways of applying conditions in R</h3>
<p>Since we’re talking about conditionals, I thought it worth mentioning two other useful ways for applying conditions to a vector or other object: <code>base::switch</code> and <code>dplyr::case_when</code>. These are great for “unnesting” complicated control flows, making code easier to read, understand, and write. Worth checking out.</p>
</div>
</div>
