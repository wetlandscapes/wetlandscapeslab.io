---
title: A brief visualization of R's distribution functions, focusing on the normal
  distribution
author: Jason Mercer
date: '2020-02-09'
slug: a-brief-visualization-of-rs-distribution-functions
categories:
  - data science
  - R
tags: []
description: 'The intent of this post is to provide a visual intuition of the various distribution functions in R, using the normal distribution as an example.'
featured: fig-1.png
featuredpath: ../blog/2020-02-09-a-brief-visualization-of-r-s-distribution-functions_files/figure-html
linktitle: ''
type: post
output: blogdown::html_page
---



<style>
  .caption {
    color: #808080 !important;
    font-style: italic !important;
  }
  h1 {
    font-size: 22px;
  }
</style>
<p>When I first started playing with the distribution functions in R I would always get confused as to what the different name combinations meant. For example, the differences between <code>rnorm()</code>, <code>dnorm()</code>, <code>dt()</code>, <code>rbinom()</code> were all pretty mysterious to me. Now I understand these names are composed of two parts:</p>
<ul>
<li>Prefix: An indication of the “map” being made (more on that in a moment)</li>
<li>Suffix: The distribution of interest.</li>
</ul>
<p>In terms of understanding the suffix, R comes pre-loaded with a bunch of typical distributions, such as (note the use of "*" as a place-holder for the prefixes of R’s distribution function names):</p>
<ul>
<li><code>*norm</code>: Normal</li>
<li><code>*t</code>: Student-t</li>
<li><code>*binom</code>: Binomial</li>
</ul>
<p>That was kind of an “A ha!” moment, and seems pretty obvious now. I should also mention a complete list of R’s built-in distributions can be found <a href="https://stat.ethz.ch/R-manual/R-devel/library/stats/html/Distributions.html">here</a>. There is also a really, really useful <a href="https://cran.r-project.org/web/views/Distributions.html">CRAN Task View focused specifically on distributions</a>.</p>
<p>What was less obvious to me was what the various prefixes on the distribution names meant. Typically, these prefixes and their meanings are ("*" simply indicates there is usually a suffix associated with these functions):</p>
<ul>
<li><code>d*</code>: Returns a density (point probability; likelihood) value, given a quantile and some set of distribution parameters.</li>
<li><code>p*</code>: Returns a cumulative probability value, given a quantile and some set of distribution parameters.</li>
<li><code>q*</code>: Returns a quantile, given a cumulative probability value and some set of distribution parameters.</li>
<li><code>r*</code>: Returns a random number (quantile), the value of which is determined by a distribution described by some set of parameters.</li>
</ul>
<p>The above meanings were not obvious to me from the help files that came with R, nor were the mathematical descriptions therein. Instead what I found to be useful were visual representations of these different relationships, which helped me better conceptualize the different “mappings” associated with the prefixes highlighted above.</p>
<p><em>Thus, the point of this post is to provide myself (and hopefully others) with a visual intuition of the various distribution functions in R, focusing on the normal distribution as an example.</em></p>
<div id="libraries" class="section level1">
<h1>Libraries</h1>
<p>We’ll use a couple of libraries to help illustrate the relationships. <code>patchwork</code> is a great library for putting together different <code>ggplot2</code> outputs that would be otherwise difficult to combine. <code>paletteer</code> is my favorite color package, as it aggregates a number of other color packages, making available a wide selection of pretty color combinations. And then the <code>tidyverse</code> loads <code>ggplot2</code>, as well as other helpful packages such as <code>dplyr</code> and <code>tidyr</code>.</p>
<pre class="r"><code>library(patchwork)
library(paletteer)
library(tidyverse)</code></pre>
</div>
<div id="making-the-data" class="section level1">
<h1>Making the data</h1>
<p>To explore the relationship between the different normal distribution functions I’ll use a standard normal distribution, which is centered on 0 (i.e., the mean is 0) and has a standard deviation of 1.</p>
<pre class="r"><code>#Data and parameters
z_scores &lt;- seq(-4, 4, by = 0.01)
mu &lt;- 0
sd &lt;- 1


#Functions
##Using `dnorm` and `pnorm` to setup the &quot;skeleton&quot; of related plots.
normal_dists &lt;- list(`dnorm()` = ~ dnorm(., mu, sd),
  `rnorm()` = ~ dnorm(., mu, sd),
  `pnorm()` = ~ pnorm(., mu, sd),
  `qnorm()` = ~ pnorm(., mu, sd))

##Apply functions to data and parameter combinations
df &lt;- tibble(z_scores, mu, sd) %&gt;%
  mutate_at(.vars = vars(z_scores), .funs = normal_dists) %&gt;%
  #&quot;Lengthen&quot; the data
  pivot_longer(cols = -c(z_scores, mu, sd), names_to = &quot;func&quot;,
    values_to = &quot;prob&quot;) %&gt;%
  #Categorize based on shape of distribution -- need to split up the dataframe
  # for plotting later.
  mutate(distribution = ifelse(func == &quot;pnorm()&quot; | func == &quot;qnorm()&quot;,
    &quot;Cumulative probability&quot;, &quot;Probability density&quot;))

##Split up the data into different pieces that can then be added to a plot.
###Probabilitiy density distrubitions
df_pdf &lt;- df %&gt;%
  filter(distribution == &quot;Probability density&quot;) %&gt;%
  rename(`Probabilitiy density` = prob)

###Cumulative density distributions
df_cdf &lt;- df %&gt;%
  filter(distribution == &quot;Cumulative probability&quot;) %&gt;%
  rename(`Cumulative probability` = prob)

###dnorm segments
  #Need to make lines that represent examples of how values are mapped -- there
  # is probably a better way to do this, but quick and dirty is fine for now.
df_dnorm &lt;- tibble(z_start.line_1 = c(-1.5, -0.75, 0.5),
  pd_start.line_1 = 0) %&gt;%
  mutate(z_end.line_1 = z_start.line_1,
    pd_end.line_1 = dnorm(z_end.line_1, mu, sd),
    z_start.line_2 = z_end.line_1,
    pd_start.line_2 = pd_end.line_1,
    z_end.line_2 = min(z_scores),
    pd_end.line_2 = pd_start.line_2,
    id = 1:n()) %&gt;%
  pivot_longer(-id) %&gt;%
  separate(name, into = c(&quot;source&quot;, &quot;line&quot;), sep = &quot;\\.&quot;) %&gt;%
  pivot_wider(id_cols = c(id, line), names_from = source) %&gt;%
  mutate(func = &quot;dnorm()&quot;,
    size = ifelse(line == &quot;line_1&quot;, 0, 0.03))

###rnorm segments
  #Make it reproducible
set.seed(20200209)
df_rnorm &lt;- tibble(z_start = rnorm(10, mu, sd)) %&gt;%
  mutate(pd_start = dnorm(z_start, mu, sd),
    z_end = z_start,
    pd_end = 0,
    func = &quot;rnorm()&quot;)

###pnorm segments
df_pnorm &lt;- tibble(z_start.line_1 = c(-1.5, -0.75, 0.5),
  pd_start.line_1 = 0) %&gt;%
  mutate(z_end.line_1 = z_start.line_1,
    pd_end.line_1 = pnorm(z_end.line_1, mu, sd),
    z_start.line_2 = z_end.line_1,
    pd_start.line_2 = pd_end.line_1,
    z_end.line_2 = min(z_scores),
    pd_end.line_2 = pd_start.line_2,
    id = 1:n()) %&gt;%
  pivot_longer(-id) %&gt;%
  separate(name, into = c(&quot;source&quot;, &quot;line&quot;), sep = &quot;\\.&quot;) %&gt;%
  pivot_wider(id_cols = c(id, line), names_from = source) %&gt;%
  mutate(func = &quot;pnorm()&quot;,
    size = ifelse(line == &quot;line_1&quot;, 0, 0.03))

###qnorm segments
df_qnorm &lt;- tibble(z_start.line_1 = min(z_scores),
  pd_start.line_1 = c(0.1, 0.45, 0.85)) %&gt;%
  mutate(z_end.line_1 = qnorm(pd_start.line_1),
    pd_end.line_1 = pd_start.line_1,
    z_start.line_2 = z_end.line_1,
    pd_start.line_2 = pd_end.line_1,
    z_end.line_2 = z_end.line_1,
    pd_end.line_2 = 0,
    id = 1:n()) %&gt;%
  pivot_longer(-id) %&gt;%
  separate(name, into = c(&quot;source&quot;, &quot;line&quot;), sep = &quot;\\.&quot;) %&gt;%
  pivot_wider(id_cols = c(id, line), names_from = source) %&gt;%
  mutate(func = &quot;qnorm()&quot;,
    size = ifelse(line == &quot;line_1&quot;, 0, 0.03))</code></pre>
</div>
<div id="plotting-the-relationships" class="section level1">
<h1>Plotting the relationships</h1>
<p>Now that the data has been made, we can put it all together using <code>ggplot2</code> and <code>patchwork</code>.</p>
<pre class="r"><code>#Plot the data
# Note: I can&#39;t combine the data in a normal facet layout, due to differences
# in axis labels, so I&#39;m using the patchwork library to bring everything
# together after the fact.

##Color palette
cp &lt;- paletteer_d(&quot;ggsci::default_locuszoom&quot;, 4, )
names(cp) &lt;- c(&quot;dnorm()&quot;, &quot;rnorm()&quot;, &quot;pnorm()&quot;, &quot;qnorm()&quot;)

##Probabilitiy density
p_pdf &lt;- df_pdf %&gt;%
  ggplot(aes(z_scores, `Probabilitiy density`)) +
  geom_segment(data = df_dnorm,
    aes(z_start, pd_start, xend = z_end, yend = pd_end),
    arrow = arrow(length = unit(df_dnorm$size, &quot;npc&quot;), type = &quot;closed&quot;),
    size = 0.8, color = cp[&quot;dnorm()&quot;]) +
  geom_segment(data = df_rnorm,
    aes(z_start, pd_start, xend = z_end, yend = pd_end),
    arrow = arrow(length = unit(0.03, &quot;npc&quot;), type = &quot;closed&quot;),
    size = 0.8, color = cp[&quot;rnorm()&quot;]) +
  geom_line(size = 0.6) +
  facet_wrap(~ func, nrow = 1) +
  theme_bw() +
  theme(panel.grid = element_blank(),
    axis.title.x = element_blank(),
    strip.background = element_blank(),
    text = element_text(family = &quot;serif&quot;, size = 14)) +
  scale_y_continuous(expand = expand_scale(c(0, 0.05))) +
  scale_x_continuous(expand = c(0.01, 0))

##Cumulative probability
p_cdf &lt;- df_cdf %&gt;%
  ggplot(aes(z_scores, `Cumulative probability`)) +
  geom_hline(yintercept = 0, color = &quot;grey&quot;) +
  geom_segment(data = df_pnorm,
    aes(z_start, pd_start, xend = z_end, yend = pd_end),
    arrow = arrow(length = unit(df_dnorm$size, &quot;npc&quot;), type = &quot;closed&quot;),
    size = 0.8, color = cp[&quot;pnorm()&quot;]) +
  geom_segment(data = df_qnorm,
    aes(z_start, pd_start, xend = z_end, yend = pd_end),
    arrow = arrow(length = unit(df_qnorm$size, &quot;npc&quot;), type = &quot;closed&quot;),
    size = 0.8, color = cp[&quot;qnorm()&quot;]) +
  geom_line(size = 0.6) +
  facet_wrap(~ func, nrow = 1) +
  labs(x = &quot;z-score/quantiles&quot;) +
  theme_bw() +
  theme(panel.grid = element_blank(),
    strip.background = element_blank(),
    text = element_text(family = &quot;serif&quot;, size = 14)) +
  scale_x_continuous(expand = c(0.01, 0))

##Combine the plots
p_pdf + p_cdf + plot_layout(ncol = 1)</code></pre>
<div class="figure"><span id="fig:fig"></span>
<img src="/blog/2020-02-09-a-brief-visualization-of-r-s-distribution-functions_files/figure-html/fig-1.svg" alt="Examples comparing the mappings between different normal distribution functions, given a mean of 0 and standard deviation of 1. Because a standard normal distribution is being used, the terms 'z-score' and 'quantile' are used interchangeably. Arrows are intended to provide a sense of direction for the examples, in terms of what data are input and output from a given function. `dnorm()` maps z-scores to their respective density values (point probabilities). `rnorm()` randomly draws quantiles, weighted by their probability -- hence why the most of the 10 random draws are clustered near the peak of the distribution. `pnorm()` maps a z-score to it's cumulative probability. `qnorm()` maps a cumulative probability to a quantile." width="100%" />
<p class="caption">
Figure 1: Examples comparing the mappings between different normal distribution functions, given a mean of 0 and standard deviation of 1. Because a standard normal distribution is being used, the terms ‘z-score’ and ‘quantile’ are used interchangeably. Arrows are intended to provide a sense of direction for the examples, in terms of what data are input and output from a given function. <code>dnorm()</code> maps z-scores to their respective density values (point probabilities). <code>rnorm()</code> randomly draws quantiles, weighted by their probability – hence why the most of the 10 random draws are clustered near the peak of the distribution. <code>pnorm()</code> maps a z-score to it’s cumulative probability. <code>qnorm()</code> maps a cumulative probability to a quantile.
</p>
</div>
<p><br />
And there we have it! A simple visualization to provide a little bit more intuition into R’s distribution functions.
<br /></p>
</div>
