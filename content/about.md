---
title: "About me"
---

<style type="text/css">
figure-right {
  position: relative;
  align: right;
  float: right;
  width: 45%;
  text-align: left;
  font-style: italic;
  font-size: smaller;
  text-indent: 0;
  border: thin silver solid;
  margin: 0.5em;
  padding: 0.5em;
}
figure-left {
  position: relative;
  align: left;
  float: left;
  width: 45%;
  text-align: left;
  font-style: italic;
  font-size: smaller;
  text-indent: 0;
  border: thin silver solid;
  margin: 0.5em;
  padding: 0.5em;
}
</style>

[//]: # (Note the "div" tags around figures. Needed to keep the figure and caption together.)
<div>
  <figure-right>
    <img src="/img/about/about-viking.jpg">
    <figcaption>
      Packrafting the Anaktuvuk River, North Slope, Alaska.
    </figcaption>
  </figure-right>
</div>

Hi! My name is Jason Mercer. I am a PhD candidate at the [University of Wyoming](http://www.uwyo.edu) in both the [Water Resources PhD Program](http://www.uwyo.edu/wrese/) and the [Dept. of Botany](http://www.uwyo.edu/botany/). I study with [Dr. Dave Williams](http://www.uwyo.edu/dgw/home.html), an expert in the field of isotope ecohydrology.

My [research page](/research/) provides some of my academic interests, but in summary, I really like lakes, wetlands, and data science. My obsession with aquatic ecosystems is probably in part due to my upbringing. I was born and raised in Alaska where > 40 % of the state is considered wetland. My professional career has also always focused on some aspect of natural resources management, so it's hard to escape working with wetlands either directly or indirectly when they are such a prominant feature on the landscape. 

<div>
  <figure-left>
    <img src="/img/about/about-soil.jpg">
    <figcaption>
      Soil plug hunting, North Slope, Alaska.
    </figcaption>
  </figure-left>
</div>

Interest in data science is a more recent development. While computers have seemingly always been in my life (my mom was a systems administrator in my younger years), I didn't do much scripting or programming until my MSc where I (re)discovered the utility of languages like R, Python, and C++ for doing data analysis. Since then, I've been virtually inhaling as much information as I can related to how I can use the programming paradigm to better understand the ecosystems I study. I'm particularly obsessed with uncertainty. In that respect I'm really fascinated with Bayesian analysis as it treats uncertainty in a way that seems more appropriate than just assuming error propogates via "quadrature" (a fancy term for assuming errors are normal). For this and other reasons I've thus become very familiar with the probabilistic programming language [Stan](https://mc-stan.org/).

Because my life currently focuses on my PhD, my extracurricular activities have somewhat changed over the last few years. However, when I can, I do enjoy hiking, skiing, cooking, drinking, friends, board games, and music.
